import datetime
"""This program is public domain."""

import os.path
import subprocess
import tempfile

import desig
import pytest

import xmlto80

dates = [
    "2013-10-04T08:05:24.576Z",     # a typical time
    "2013-10-04T08:05:24Z",         # truncated to whole second
    "2013-10-04T08:05:23.999999Z",  # a microsecond before
    "2013-01-01T00:00:00Z",         # start of year
    "2013-01-01T00:00:01Z",         # a second later
    "2012-12-31T23:59:59Z",         # a second before
    "2013-01-01T00:00:00.25Z",       # just a quarter second later
    "2012-12-31T23:59:59.75Z",       # a quarter second before
    "2012-02-29T00:00:01Z",         # leap day
    "2012-02-28T23:59:59Z",         # a second before
    "2012-02-29T23:59:59Z",         # last second of leap day
]

# two decimals of seconds of ra is 1/360000 hr of ra or 15/360000 = .00004 deg.
ras = [
    "0",
    "0.00004",
    "0.00002",
    "359.99996",
    "359.99998",
]

# one decimal of seconds of dec is 1/36000 deg = .00003 deg
decs = [
    "0",
    "0.00003",
    "0.00001",
    "-33.3",
    "-3.3",
    "-0.3",
    "-0.03",
    "-0.003",
    "-0.0003",
    "-0.00003",
    "-0.00001",
    "90",
    "89.99999",
    "89.99997",
    "-90",
    "-89.99999",
    "-89.99997",
]

mags = [
    ('', '      '),        # empty field (although might not be xsd:decimal)
    ('     ', '      '),   # blank field (although might not be xsd:decimal)
    ('19.3', '19.3  '),    # most typical
    ('19.0', '19.0  '),    # zero preserved
    ('19.31', '19.31 '),   # two decimal places
    ('19.', '19.   '),     # none
    ('19', '19    '),
    ('9', ' 9    '),
    ('9.', ' 9.   '),
    ('-1', '-1    '),
    ('-1.2', '-1.2  '),
    ('-1.23', '-1.23 '),
    ('-1.238', '-1.24 '),  # rounding
    ('19.238', '19.24 '),
    ('99.99', '99.99 '),
    ('99.997', '      '),  # overflow
    ('-9.997', '      '),
    ('.', '  .   '),       # more weird cases
    ('.0', '  .0  '),
    ('-0', '-0    '),
    ('09', '09    '),
]


def parse8601(ds):
    """parse ades 8601 format, return a Python datetime.

    xmlto80 uses dateutil.  here is a different implementation using just
    the standard library
    """
    return datetime.datetime(
        int(ds[:4]), int(ds[5:7]), int(ds[8:10]),
        int(ds[11:13]), int(ds[14:16]), int(ds[17:19]),
        int(float(ds[19:-1]) * 1e6 + .5) if ds[19] == '.' else 0,
        datetime.timezone.utc)


def parse80date(ds):
    """parse 80 column format, return a Python datetime."""
    return datetime.datetime(int(ds[:4]), int(ds[5:7]), int(ds[8:10]),
        tzinfo=datetime.timezone.utc) + datetime.timedelta(float(ds[10:]))


def test_date():
    """a day is 24*60*60 = 86400 seconds. .00001 day is a little less than
    a second.  so round trip from 8601 to 80 column with 5 decimal digits
    should come within half a second.
    """
    for ds in dates:
        # test lp date
        d80 = xmlto80.date(False, {'obsTime': ds})
        dt = parse8601(ds) - parse80date(d80)
        assert len(d80) == 17
        assert d80[16] == ' '
        assert(abs(dt.total_seconds()) <= .5)

        # test hp date
        d80 = xmlto80.date(True, {'obsTime': ds})
        dt = parse8601(ds) - parse80date(d80)
        assert len(d80) == 17
        assert d80[16] != ' '
        assert(abs(dt.total_seconds()) <= .05)


def parse80ra(rs):
    """parse 80 column format, return float seconds of RA"""
    return (int(rs[:2]) * 60 + int(rs[3:5])) * 60 + float(rs[6:])


def test_ra():
    for rs in ras:
        # test lp ra
        r80 = xmlto80.ra(False, {'ra': rs})
        assert(r80[3:5] != '60')
        assert(r80[6:8] != '60')
        assert(len(r80) == 12)
        assert r80[11] == ' '
        # compare seconds of ra.  round trip difference should be within .005
        dr = float(rs) * 3600 / 15 - parse80ra(r80)
        assert((dr + .005) % 86400 < .01)

        # test hp ra
        r80 = xmlto80.ra(True, {'ra': rs})
        assert(r80[3:5] != '60')
        assert(r80[6:8] != '60')
        assert(len(r80) == 12)
        assert r80[11] != ' '
        # compare seconds of ra.  round trip difference should be within .0005
        dr = float(rs) * 3600 / 15 - parse80ra(r80)
        assert((dr + .0005) % 86400 < .001)


def parse80dec(ds):
    """parse 80 column format, return float seconds of dec"""
    d = (int(ds[1:3]) * 60 + int(ds[4:6])) * 60 + float(ds[7:])
    if ds[0] == '-':
        return -d
    return d


def test_dec():
    for ds in decs:
        # test lp dec
        d80 = xmlto80.dec(False, {'dec': ds})
        assert(d80[3:5] != '60')
        assert(d80[6:8] != '60')
        assert(len(d80) == 12)
        assert d80[11] == ' '
        # compare seconds of dec.  round trip difference should be within .05
        d = float(ds) * 3600 - parse80dec(d80)
        assert(abs(d) <= .05)

        # test hp dec
        d80 = xmlto80.dec(True, {'dec': ds})
        assert(d80[3:5] != '60')
        assert(d80[6:8] != '60')
        assert(len(d80) == 12)
        assert d80[11] != ' '
        # compare seconds of dec.  round trip difference should be within .005
        d = float(ds) * 3600 - parse80dec(d80)
        assert(abs(d) <= .005)


def test_mag():
    for mt in mags:
        assert xmlto80.magband({'mag': mt[0]}) == mt[1]


def test_minimal():
    with tempfile.TemporaryDirectory() as td:
        o = os.path.join(td, 'min.obs')
        xmlto80.to80(o, 'minimal.xml')
        assert diff(o, 'minimal.ref') == 0


def test_full():
    with tempfile.TemporaryDirectory() as td:
        o = os.path.join(td, 'full.obs')
        xmlto80.to80(o, 'full.xml', 'full.json')
        assert diff(o, 'full.ref') == 0


def test_tel():
    with tempfile.TemporaryDirectory() as td:
        o = os.path.join(td, 'tel.obs')
        xmlto80.to80(o, 'tel.xml')
        assert diff(o, 'tel.ref') == 0


def test_errors():
    with tempfile.TemporaryDirectory() as td:
        o = os.path.join(td, 'bad.obs')
        x = os.path.join(td, 'bad.xml')

        with open(x, 'w') as f:
            f.write('<ades />')
        with pytest.raises(ValueError) as e:
            xmlto80.to80(o, x)
        assert str(e.value) == 'no version element'

        with open(x, 'w') as f:
            f.write('<ades version="bad"></ades>')
        with pytest.raises(ValueError) as e:
            xmlto80.to80(o, x)
        assert str(e.value) == 'not version 2017'

        with open(x, 'w') as f:
            f.write('<ades version="2017"><bad /></ades>')
        with pytest.raises(ValueError) as e:
            xmlto80.to80(o, x)
        assert str(e.value) == 'expected obsBlock'

        with open(x, 'w') as f:
            f.write('<ades version="2017"><obsBlock /></ades>')
        with pytest.raises(ValueError) as e:
            xmlto80.to80(o, x)
        assert str(e.value) == 'obsBlock must have obsContext and obsData elements'

        with open(x, 'w') as f:
            f.write("""
<ades version="2017"><obsBlock>
  <obsContext />
  <obsData />
</obsBlock></ades>""")
        with pytest.raises(ValueError) as e:
            xmlto80.to80(o, x)
        assert str(e.value) == 'expected observatory mpcCode'

        with open(x, 'w') as f:
            f.write("""
<ades version="2017"><obsBlock>
  <obsContext><observatory><mpcCode /></observatory></obsContext>
  <obsData><position /></obsData>
</obsBlock></ades>""")
        with pytest.raises(ValueError) as e:
            xmlto80.to80(o, x)
        assert str(e.value) == 'position unsupported'

        with open(x, 'w') as f:
            f.write("""
<ades version="2017"><obsBlock>
  <obsContext><observatory><mpcCode /></observatory></obsContext>
  <obsData><optical>
    <sys>WGS84</sys>
    <astCat />
  </optical></obsData>
</obsBlock></ades>""")
        with pytest.raises(ValueError) as e:
            xmlto80.to80(o, x)
        assert str(e.value) == 'sys WGS84 unimplemented'

        with open(x, 'w') as f:
            f.write('<ades version="2017" />')
        with pytest.raises(Exception) as e:
            xmlto80.Scan(x).write80('')
        assert str(e.value) == 'bug.  line has 0 columns'

        with pytest.raises(desig.DesigError) as e:
            xmlto80.des12({'permID': '321', 'provID': 'P/2010 QR'})
        assert str(e.value) == 'inconsistent permID and provID'

        with pytest.raises(desig.DesigError) as e:
            xmlto80.des12({'permID': '321', 'provID': 'S/2001 U 9'})
        assert str(e.value) == 'inconsistent permID and provID'

        with pytest.raises(ValueError) as e:
            xmlto80.c15({'mode': 'bad'})
        assert str(e.value) == 'mode not supported'

        xmlto80.km11(99999)
        with pytest.raises(ValueError) as e:
            xmlto80.km11(100000)
        assert str(e.value) == 'space-based coordinates out of range for 80 column format'

        xmlto80.au11(9)
        with pytest.raises(ValueError) as e:
            xmlto80.au11(10)
        assert str(e.value) == 'space-based coordinates out of range for 80 column format'


def diff(f1, f2):
    try:
        subprocess.check_output(['diff', f1, f2])
        return 0
    except subprocess.CalledProcessError as e:  # pragma: no cover
        print(e.output.decode())
        return 1
