#!/usr/bin/env python3
"""This program is public domain."""

import pytest
import desig

perms = [
    ('00001', 0, '1'),
    ('00433', 0, '433'),
    ('99999', 0, '99999'),
    ('A0000', 0, '100000'),
    ('D4340', 0, '134340'),
    ('Z9999', 0, '359999'),
    ('a0000', 0, '360000'),
    ('z9999', 0, '619999'),
    ('0001P', '       ', '1P'),
    ('0257P', '       ', '257P'),
    ('0073P', '      c', '73P-C'),
    ('0073P', '     ac', '73P-AC'),
    ('J013S', 0, 'Jupiter 13'),
]

provs = [
    ('K14A00A', 0, '2014 AA'),
    ('K14A01A', 0, '2014 AA1'),
    ('K14Aa0A', 0, '2014 AA360'),
    ('K14Az9A', 0, '2014 AA619'),
    ('PLS2001', 0, '2001 P-L'),
    ('T2S2801', 0, '2801 T-2'),
    ('J98Q54P', '    PJ98Q54P', 'P/1998 QP54'),
    ('J97B06A', '    CJ97B06A', 'C/1997 BA6'),
    ('J99K070', '    CJ99K070', 'C/1999 K7'),
    ('J31A00N', '    CJ31A00N', 'C/1931 AN'),
    ('I86S010', '    PI86S010', 'P/1886 S1'),
    ('J94P01b', '    PJ94P01b', 'P/1994 P1-B'),
    ('J96J01a', '    CJ96J01a', 'C/1996 J1-A'),
    ('K01S310', 0, 'S/2001 S 31'),
    ('K01U090', 0, 'S/2001 U 9'),
]


def testPerm():
    for pt in perms:
        p = desig.parsePermU(pt[2])
        assert p.packed5 == pt[0]
        if pt[1] != 0:
            assert p.packedFrag == pt[1]

    with pytest.raises(desig.DesigError) as e:
        desig.parsePermU("")
    assert str(e.value) == 'invalid permID '

    with pytest.raises(desig.DesigError) as e:
        desig.parsePermU("0")
    assert str(e.value) == 'invalid minor planet number 0'

    with pytest.raises(desig.DesigError) as e:
        desig.parsePermU("-1")
    assert str(e.value) == 'invalid minor planet number -1'

    d = desig.parsePermU("620000")
    with pytest.raises(desig.DesigError) as e:
        d.packed5
    assert str(e.value) == 'minor planet number 620000 too large for 5 character packed format'

    d = desig.parsePermU("73P-ABC")
    with pytest.raises(desig.DesigError) as e:
        d.packedFrag
    assert str(e.value) == '3 character fragment invalid for packed format'


def testProv():
    for pt in provs:
        p = desig.parseProvU(pt[2])
        assert p.packed7 == pt[0]
        if pt[1] != 0:
            assert p.packed12 == pt[1]

    d = desig.parseProvU("2022 AA620")
    with pytest.raises(desig.DesigError) as e:
        d.packed7
    assert str(e.value) == '2022 AA620 too large for 7 character packed format'

    d = desig.parseProvU("C/2022 AA620")
    with pytest.raises(desig.DesigError) as e:
        d.packed7
    assert str(e.value) == 'C/2022 AA620 too large for 7 character packed format'

    d = desig.parseProvU("P/1998 QP54-C")
    with pytest.raises(desig.DesigError) as e:
        d.packed7
    assert str(e.value) == "P/1998 QP54-C with MP desig and frag can't be packed"

    d = desig.parseProvU("C/1996 J1-AC")
    with pytest.raises(desig.DesigError) as e:
        d.packed7
    assert str(e.value) == "C/1996 J1-AC can't pack 2 character fragment with provisional designation"

    d = desig.parseProvU("S/2022 J 620")
    with pytest.raises(desig.DesigError) as e:
        d.packed7
    assert str(e.value) == 'S/2022 J 620 too large for 7 character packed format'

    with pytest.raises(desig.DesigError) as e:
        desig.parseProvU("SPEKTR-R")
    assert str(e.value) == 'invalid provID SPEKTR-R'
