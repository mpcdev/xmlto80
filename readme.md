# XMLto80

This source repository contains the program `xmlto80.py` as used by the
Minor Planet Center.  XMLto80 converts ADES format observations to an
80 column format for use by MPC programs that require the 80 column format.

XMLto80 exists as an interim utility for internal use at the MPC, and may or
may not have more general utility.  The source code is provided for
transparency into this transitional submission process.  Observers or
submitters will not generally need to actually install and run this software.
In particular, if you can produce ADES format observations, you should submit
your observations in the ADES format.

## Prerequisites

This is Python 3 code.  Python 3 is required to run the code.

XMLto80 imports `lxml` and `dateutil`, available at
https://pypi.python.org/pypi/lxml and
https://pypi.python.org/pypi/python-dateutil.

## Installation

Other than installing the above prerequisites, there is no special
installation.  If you downloaded or cloned this source repository you can
probably just run xmlto80.py as it is.

## Command line use

As documented inside xmlto80.py, the program can be run as a command line
script.

    ./xmlto80.py <output fn> <input xml fn> [input json fn]

The output fn is path and file name for the 80 column output.  Any existing
file will be overwritten.  (It's not overwritten until after the input xml
is successfully read.)

The input xml should be valid by the schema "submit.xsd" or similar.

The optional input json should be a json object with keys "ack" and "ac2."

## Examples

The file minimal.xml is a minimal ADES XML file for example purposes.

    ./xmlto80.py minimal.obs minimal.xml

This command will run the program, producing minimal.obs as an 80 column
output file.  The output should be identical to the file minimal.ref in
this repository.

The file full.xml is a larger ADES XML file, to exercise more capabilities
of the program.  One capability is to add certain information which can be
submitted in the header of the 80 column format but which is excluded from
the ADES format.  This information is added from JSON format as an optional
argument:

    ./xmlto80.py full.obs full.xml full.json

The output file full.obs should be identical to the file full.ref in this
repository.

## Conversion notes

In general, conversion is lossy but this should not be a concern.  Submitted
ADES observations are always archived in their full original format.  All
submitted information will remain available and will be used by the MPC as
processes are upgraded.

### Designations

Values for the permanent designation field, columns 1-5, and the provisional
designation field, columns 6-12, are derived from the ADES fields `permID`,
`provID`, and `trkSub`.  Where designations cannot be used directly, unique
temporary designations are generated.  These will begin with a lower case 'x'
and may incorporate some characters of the provided designation that could
not be used directly.

### Astrometry precision

The ADES fields `obsTime`, `ra`, and `dec` require unit conversion and the
result must be stored to some decimal precision.  Standard precisions of 80
column dates, RAs and decs are 5, 2, and 1 decimal digits respectively, but the
format allows for one additional digit "when justified."  ADES allows for a
measure of the random error components of astrometry to be submitted as `rmsRA`
and `rmsDec`.  Where both `rmsRA` and `rmsDec` indicate that the 80 column
extra digit is justified by accuracy, the extra digit of precision is output
for all three 80 column fields of date, RA, and Dec.

### Magnitude precision

Magnitudes do not require unit conversion and the magnitude field is copied
directly when possible.  Values are rounded to two decimal places if needed.

## Tests

Tests are in files named with _test and can be run with `pytest`, available
at https://pypi.python.org/pypi/pytest.

## Copyright, license

All content in this source repository is public domain.
