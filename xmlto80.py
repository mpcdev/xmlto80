#!/usr/bin/env python3
"""Script to convert ADES XML to MPC 80 column format

Usage as a script: ./xmlto80-dev.py <output fn> <input xml fn> [input json fn]

The output fn is path and file name for the 80 column output.  Any existing
file will be overwritten.  (It's not overwritten until after the input xml
is successfully read.)

The input xml should be valid by the schema "submit.xsd" or similar.
Leap seconds not handled.  ADES and submit.xsd allow observations on
leap seconds but the code here currently raises an exception for observations
on leap seconds.

The optional input json should be a json object with keys "ack" and "ac2."

This program is public domain.
"""

import argparse
import json
import math
import sys
import unicodedata

import datetime
import dateutil.parser
from lxml import etree

import desig

# List of magbands that get modified in ADES->obs80 conversion

new_style_bands = ['Vj', 'Rc', 'Ic', 'Bj', 'Uj', 'Sg', 'Sr', 'Si', 'Sz', 'Pg', 'Pr', 'Pi', 'Pz', 'Pw', 'Ao', 'Ac', 'Gb', 'Gr']
old_style_bands = ['V', 'R', 'I', 'B', 'U', 'g', 'r', 'i', 'z', 'g', 'r', 'i', 'z', 'w', 'o', 'c', 'G', 'G']

def to80(obs, ades, meta=None, prec=None):
    """
    obs: output: file name and path for 80 column output
    ades: input:  file name and path of ades xml validating by submit.xsd
    meta: input:  file name and path of json meta data
    """
    Scan(ades).to80(obs, meta, prec)


class Scan:
    def __init__(self, fn):
        t = etree.parse(open(fn, encoding='utf-8'))
        self.root = t.getroot()
        v = self.root.get('version')
        if not v:
            raise ValueError('no version element')
        if v != '2017':
            raise ValueError('not version 2017')

        self.tmps = set()
        self.idRepl = {}
        for b in self.root:
            if b.tag != 'obsBlock':
                raise ValueError('expected obsBlock')
            if len(b) != 2 or b[0].tag != 'obsContext' or b[1].tag != 'obsData':
                raise ValueError('obsBlock must have obsContext and obsData elements')
            self.scanData(b[1])

    def scanData(self, d):
        for e in d:
            if e.tag == 'optical':
                self.scanOptical(e)
            else:
                raise ValueError(e.tag + " unsupported")

    def scanOptical(self, o):
        f = {e.tag: e.text for e in o}
        try:
            self.net
        except AttributeError:
            self.net = f['astCat']  # astCat required by submit.xsd
        try:
            self.tmps.add(des12(f)[5:])
        except:
            pass

    def to80(self, obs, meta, prec):
        self.oFile = open(obs, mode='w')
        try:
            m = json.load(open(meta, encoding='utf-8'))
        except:
            m = {}
        self.prec = prec
        for b in self.root:
            self.context(b[0], m)
            self.data(b[1])
            m = {}

    def context(self, c, m):
        cod = c.find('observatory/mpcCode')
        if cod is None:
            raise ValueError('expected observatory mpcCode')
        print('COD', cod.text, file=self.oFile)
        for e in c:
            if e.tag == 'observatory':
                self.observatory(e)
            elif e.tag == 'submitter':
                self.submitter(e)
            elif e.tag == 'observers':
                self.names('OBS', e)
            elif e.tag == 'measurers':
                self.names('MEA', e)
            elif e.tag == 'telescope':
                self.telescope(e)
            elif e.tag == 'software':
                self.comment(e)
            elif e.tag == 'coinvestigators':
                self.names('COM', e)
            elif e.tag == 'collaborators':
                self.names('COM', e)
            elif e.tag == 'fundingSource':
                self.com(e)
            elif e.tag == 'comment':
                self.comment(e)
        print('NET', self.net, file=self.oFile)  # astCat required
        for k, v in sorted(m.items()):
            if k == 'ack':
                print('ACK', v, file=self.oFile)
            elif k == 'ac2':
                print('AC2', v, file=self.oFile)
            else:
                print('COM', v, file=self.oFile)

    def data(self, d):
        for e in d:
            self.optical(e)  # optical verified in scan

    def optical(self, o):
        #  gather xml tags into a Python dictionary
        f = {e.tag: e.text for e in o}

        #  resolve designation
        try:
            d12 = des12(f)
        except desig.DesigError:
            key = (f.get('trkSub', ''),
                f.get('provID', ''), f.get('permID', ''))
            try:
                d12 = self.idRepl[key]
            except KeyError:
                d12 = self.newTmp(key)
                self.idRepl[key] = d12

        # Decide on extra digit for astrometry.  It can be forced by command
        # line switch or determined by ADES fields rmsRA and rmsDec.  For time
        # precision, ADES simply says precision "should be appropriate for the
        # astrometric accuracy."  So the rule is here is that if an extra
        # digit is justified by both rmsRA and rmsDec, that the extra digit
        # is formatted for all three.  Also for RA, just test if extra digit
        # is justified at the equator.
        hp = self.prec == "high"
        if not self.prec:
            try:
                # schema should exclude 0s in both rmsRA and rmsDec
                if float(f['rmsDec']) <= .2 and float(f['rmsRA']) / 15 <= .02:
                    hp = True
            except (KeyError, ValueError):
                pass

        # handle space based and optical
        try:
            sys = f['sys']

            #z++ 2018-09-19
            # roving obs:
            if sys == 'WGS84':
                # roving observer.  output two records.  first is same as ground
                # based except that a "V/v" is stored in column 15.
                pos1 = float(f['pos1'])
                pos1_prec = len(f['pos1']) - f['pos1'].find('.') - 1

                if pos1 < 0:
                  pos1 = 360 + pos1

                f['pos1'] = f"{pos1:>9.{pos1_prec}f}"
                f['pos3'] = f"{round(float(f['pos3']))}"

                self.write80('{}{}{}V{}{}{}         {}      {}'.format(d12,
                    disc(f), c14(f), date(hp, f), ra(hp, f), dec(hp, f),
                    magband(f), f['stn'][:3]))
                self.write80(f"{d12}  v{date(hp, f)}1{f['pos1']:<10}{f['pos2']:>10} {f['pos3']:>7}                {f['stn'][:3]}")
            elif sys == 'ICRF_KM' or sys == 'ICRF_AU':
                # space based.  output two records.  first is same as ground
                # based except that an "S" is stored in column 15.
                self.write80('{}{}{}S{}{}{}         {}      {}'.format(d12,
                    disc(f), c14(f), date(hp, f), ra(hp, f), dec(hp, f),
                    magband(f), f['stn'][:3]))
                self.write80('{}  s{}{} {} {} {}        {}'.format(d12, date(hp, f),
                    c33(sys), c35(sys, f), c47(sys, f), c59(sys, f), f['stn'][:3]))

            else:
                raise ValueError('sys {} unimplemented'.format(sys))

        except KeyError:
            # no sys means ground based
            self.write80('{}{}{}{}{}{}{}         {}      {}'.format(d12,
                disc(f), c14(f), c15(f), date(hp, f), ra(hp, f), dec(hp, f),
                magband(f), f['stn'][:3]))

    def write80(self, s):
        if len(s) != 80:
            raise ValueError('bug.  line has {} columns'.format(len(s)))
        print(s, file=self.oFile)

    def newTmp(self, key):
        """return 12 char desig field with new temp id."""
        base = an6(key[0]) or an6(key[1]) or an6(key[2])
        tmp = w7('x', base)
        seq = 0
        while tmp in self.tmps:
            seq += 1
            tmp = w7('x'+str(seq), base)
        self.tmps.add(tmp)
        return '     '+tmp

    def telescope(self, t):
        # aperture, design, detector required by schema
        # fRatio, arraySize included if present
        # name, filter, pixelScale ignored
        m = {e.tag: e.text for e in t}
        print('TEL', m['aperture'] + '-m', end='', file=self.oFile)
        try:
            f = m['fRatio']
            print(' f/' + f, end='', file=self.oFile)
        except KeyError:
            pass
        print('', norm(m['design']), '+', end='', file=self.oFile)
        try:
            a = m['arraySize']
            print('', a, end='', file=self.oFile)
        except KeyError:
            pass
        print('', norm(m['detector']), file=self.oFile)


    def names(self, hdr, e):
        line = hdr
        for n in e:
            a = norm(n.text)
            if len(line) == 3:
                line += ' '+a
                continue
            if len(line)+len(a) < 78:
                line += ', '+a
                continue
            print(line, file=self.oFile)
            line = hdr+' '+a
        print(line, file=self.oFile)

    def comment(self, c):
        for l in c:
            self.com(l)

    def con(self, e):
        print('CON', norm(e.text), file=self.oFile)

    def com(self, e):
        print('COM', norm(e.text), file=self.oFile)

    def observatory(self, o):
        for e in o:
            if e.tag != 'mpcCode':
                self.com(e)

    def submitter(self, s):
        for e in s:
            if e.tag == "name":
                self.con(e)
            else:
                self.com(e)


def norm(s):
    return str(unicodedata.normalize('NFKD', s).encode('ascii', 'ignore'), 'utf-8')


def an6(id):
    id = id.encode('ascii', 'ignore').decode()
    return ''.join(c for c in id if c.isalnum())


def w7(seq, base):
    return seq+base[len(seq)-7:]


def des12(f):
    """return 12 character designation, some combination of permID, provID,
    and trkSub."""
    cmt = False
    sat = False
    d5 = '     '
    # look for perm first
    try:
        # if the key is present, the value must parse to continue in this
        # function.  if the value doesn't parse, the exception is passed
        # to the caller.
        perm = desig.parsePermU(f['permID'])
        cmt = type(perm).__name__ == 'PermC'
        sat = type(perm).__name__ == 'PermS'
        d5 = perm.packed5
    except KeyError:
        pass
    # then look for prov
    try:
        # if the key is present, the value must parse to continue in this
        # function.  if the value doesn't parse, the exception is passed
        # to the caller.
        prov = desig.parseProvU(f['provID'])
        # now different handling for the different result types:
        if type(prov).__name__ == 'ProvC':  # comet
            if d5 == '     ':
                return prov.packed12
            if not cmt:
                raise desig.DesigError('inconsistent permID and provID')
            return d5 + prov.packed7
        if type(prov).__name__ == 'ProvS':  # natural satellite
            if d5 == '     ':
                return '    S' + prov.packed7
            if not sat:
                raise desig.DesigError('inconsistent permID and provID')
            return d5 + prov.packed7
        # prov is a minor planet type
        return d5 + prov.packed7
    except KeyError:
        pass
    # with no prov, take a trkSub
    try:
        t = f['trkSub']
    except KeyError:
        return d5 + '       '
    if len(t) > 7:
        raise desig.DesigError('trkSub too long')
    return d5 + '{:<7}'.format(t)


def disc(f):
    try:
        return f['disc']  # schema limits this to a single character
    except KeyError:
        return ' '


def c14(f):
    """note"""
    try:
        return f['notes'][:1]  # only room for one
    except (KeyError, ValueError):
        return ' '


def c15(f):
    """mode"""
    if f['mode'] == 'CCD':
        return 'C'
    raise ValueError('mode not supported')


def date(hp, f):
    dt = dateutil.parser.parse(f['obsTime'])

    #z-+ dPrec = 6    #  if hp else 5
    dPrec = 5
    bits = f['obsTime'].split(':')
    if "." in bits[-1]:
        dPrec = 6

    res = 10**dPrec
    pad = ' '*(6-dPrec)
    # compute day fraction as an integer rounded to resolution
    frac = int((dt - datetime.datetime.combine(dt.date(),
        datetime.time(tzinfo=dt.tzinfo))) / datetime.timedelta(days=1/res) + .5)
    # except if it rounds up to the full resolution, roll the day over and
    # set the fraction to zero.
    if frac == res:
        dt = dt + datetime.timedelta(days=1)
        frac = 0
    return dt.strftime('%Y %m %d')+'.{:0{p}d}{}'.format(frac, pad, p=dPrec)


def c33(sys):
    if sys == 'ICRF_KM':
        return '1'
    return '2'


def c35(sys, f):
    if sys == 'ICRF_KM':
        return km11(f['pos1'])
    return au11(f['pos1'])


def c47(sys, f):
    if sys == 'ICRF_KM':
        return km11(f['pos2'])
    return au11(f['pos2'])


def c59(sys, f):
    if sys == 'ICRF_KM':
        return km11(f['pos3'])
    return au11(f['pos3'])


def km11(d):
    d = float(d)
    if d < 0:
        n = '-'
        d = -d
    else:
        n = '+'
    s = '{}{:10.4f}'.format(n, d)
    if len(s) != 11:
      s = '{}{:10.3f}'.format(n, d)
    if len(s) != 11:
        raise ValueError(
            'space-based coordinates out of range for 80 column format')
    return s


def au11(d):
    d = float(d)
    if d < 0:
        n = '-'
        d = -d
    else:
        n = '+'
    s = '{}{:10.8f}'.format(n, d)
    if len(s) != 11:
        raise ValueError(
            'space-based coordinates out of range for 80 column format')
    return s


def ra(hp, f):
    sPrec = 3 if hp else 2
    sScale = 10**sPrec
    mScale = sScale * 60
    hScale = mScale * 60
    dScale = hScale // 15
    pad = ' '*(3-sPrec)
    i = int(float(f['ra'])*dScale+.5) % (dScale * 360)
    m = i % hScale
    s = m % mScale
    return '{:02d} {:02d} {:02d}.{:0{p}d}{}'.format(
        i//hScale, m//mScale, s//sScale, s % sScale, pad, p=sPrec)


def dec(hp, f):
    sPrec = 2 if hp else 1
    sScale = 10**sPrec
    mScale = sScale * 60
    dScale = mScale * 60
    pad = ' '*(2-sPrec)
    # format dec value
    i = int(math.floor(float(f['dec'])*dScale+.5))
    if i < 0:
        g = '-'
        i = -i
    else:
        g = '+'
    m = i % dScale
    s = m % mScale
    return '{}{:02d} {:02d} {:02d}.{:0{p}d}{}'.format(
        g, i//dScale, m//mScale, s//sScale, s % sScale, pad, p=sPrec)


nomagband = '      '


def magband(f):
    try:
        b = f['band']
        if len(b) > 1:
            if b in new_style_bands:
                i = new_style_bands.index(b)
                b = old_style_bands[i] 
        if len(b) > 1:
            return nomagband
    except KeyError:
        b = ' '

    try:
        # this is supposed to be already validated to xsd:decimal
        m = f['mag'].strip()
    except KeyError:
        return nomagband

    try:
        p = m.index('.')
    except ValueError:  # no decimal point
        if len(m) > 2:
            return nomagband
        return ' '*(2-len(m))+m+'   '+b

    # if more than two places after decimal point, try rounding to two
    if len(m)-(p+1) > 2:
        f = '{:5.2f}'.format(float(m))
        if len(f) > 5:
            return nomagband
        return f+b

    # if >2 characters before dp, toss.
    if p > 2:
        return nomagband

    # just pad anything else
    return '{:5}{}'.format(' '*(2-p)+m, b)


if __name__ == '__main__':  # pragma: no cover
    parser = argparse.ArgumentParser()
    parser.add_argument("out", help="output file name, 80-column format")
    parser.add_argument("xml", help="input file name, ADES XML format")
    parser.add_argument("json",
            help="submission metadata, JSON format (optional)", nargs='?')
    parser.add_argument("-p", "--precision", choices=["low", "high"],
            help="force output precision for date, RA, and dec")
    args = parser.parse_args()
    to80(args.out, args.xml, meta=args.json, prec=args.precision)
