#!/usr/bin/env python3
"""Classes reprsenting minor planet, comet, and natural satellite designations.

Parsers parse designations into components and instantiate designation objects,
then properties on those objects return the designations formatted in various
packed and unpacked formats.

There are two parsers, one for unpacked permanent designations and one for
unpacked temporary designations.  These are the parsers needed to parse ADES
permID and provID elements.  Methods expect clean data such as would pass XML
schema validation.

Returned permanent designation objects are one of three classes, for minor
planets, comets, and natural satellites.

Returned provisional designation objects are one of five, for modern minor
planet designations, PL survey designations, Trojan survey designations,
comet designations, and natural satellite designations.

These various classes represent the parsed components so they can be easily
assembled into a requested designation format.  Implemented are just the
formats needed for the 80 column observation format.

This program is public domain.
"""

import re
import string


class DesigError(Exception):
    pass


class PermM:
    """Permanent minor planet designations"""
    def __init__(self, num):
        if num <= 0:
            raise DesigError('invalid minor planet number {}'.format(num))
        self.num = num

    @property
    def packed5(self):
        if self.num > 619999:
            raise DesigError('minor planet number {} too large for 5 character packed format'.format(self.num))
        if self.num < 100000:
            return '{:0>5d}'.format(self.num)
        l = self.num//10000 - 10
        if l < 26:
            l = chr(ord('A')+l)
        else:
            l = chr(ord('a')+l-26)
        return '{}{:04d}'.format(l, self.num % 10000)


class PermC:
    """Permanent comet designations"""
    def __init__(self, num, typ='P', frag=None):
        self.num = int(num)
        self.typ = typ
        self.frag = frag

    @property
    def packed5(self):
        return '{:04d}{}'.format(self.num, self.typ)

    @property
    def packedFrag(self):
        """returns 7 character field that allows for 2 character fragments"""
        if not self.frag or self.frag.isspace():
            return '       '
        if len(self.frag) > 2:
            raise DesigError('{} character fragment invalid for packed format'.format(len(self.frag)))
        return '{:>7}'.format(self.frag.lower())


class PermS:
    """Permanent natural satelllite designations"""
    def __init__(self, planet, num):
        self.planet = planet
        self.num = int(num)

    @property
    def packed5(self):
        return '{}{:03d}S'.format(self.planet[0], self.num)


rxPermC = re.compile(r'(\d+)([PD])(-([A-Z]+))?')
rxPermS = re.compile(r'(Jupiter|Saturn|Uranus|Neptune) (\d+)')


def parsePermU(u):
    """parse an unpacked permanent designation

    Parameters
    ----------
    u : string
        minor panet, comet, or satellite designation in unpacked form

    Returns
    -------
    MPPerm, CPerm, or SPerm
    """
    try:  # most common case first
        return PermM(int(u))
    except ValueError:
        pass
    # comets probably next most likely
    m = rxPermC.fullmatch(u)
    if m:
        return PermC(*m.group(1, 2, 4))
    # satellite?
    m = rxPermS.fullmatch(u)
    if m:
        return PermS(*m.groups())
    raise DesigError('invalid permID ' + u)


n62 = string.digits + string.ascii_uppercase + string.ascii_lowercase


class ProvM:
    """Provisional modern minor planet designations"""
    def __init__(self, cen, yr, hm, om, num):
        self.cen = int(cen)
        self.yr = int(yr)
        self.hm = hm
        self.om = om
        self.num = int(num) if num > '' else 0

    @property
    def unpacked(self):
        return '{:02d}{:02d} {}{}{}'.format(
            self.cen, self.yr, self.hm, self.om,
            str(self.num) if self.num > 0 else '')

    @property
    def packed7(self):
        try:
            return '{}{:02d}{}{}{}{}'.format(
                chr(self.cen - 10 + ord('A')),
                self.yr,
                self.hm,
                n62[self.num // 10],
                self.num % 10,
                self.om)
        except IndexError:
            raise DesigError(self.unpacked + " too large for 7 character packed format")


class ProvP:
    """Provisional P-L survey minor planet designations"""
    def __init__(self, num):
        self.num = int(num)

    @property
    def packed7(self):
        return 'PLS{:04d}'.format(self.num)


class ProvT:
    """Provisional Trojan survey minor planet designations"""
    def __init__(self, num, survey):
        self.num = int(num)
        self.survey = survey

    @property
    def packed7(self):
        return 'T{}S{:04d}'.format(self.survey, self.num)


class ProvC:
    """Provisional comet designations"""
    def __init__(self, orbit, cen, yr, hm, om, num, frag):
        self.orbit = orbit
        self.cen = int(cen)
        self.yr = int(yr)
        self.hm = hm
        self.om = om
        self.num = int(num) if num > '' else 0
        self.frag = frag

    @property
    def unpacked(self):
        return '{}/{:02d}{:02d} {}{}{}{}'.format(
            self.orbit, self.cen, self.yr, self.hm,
            self.om if self.om else '',
            str(self.num) if self.num > 0 else '',
            '-'+self.frag if self.frag else '')

    @property
    def packed7(self):
        try:
            tens = n62[self.num // 10]
        except IndexError:
            raise DesigError(self.unpacked + " too large for 7 character packed format")

        if self.om:
            if self.frag:
                raise DesigError(self.unpacked +
                    " with MP desig and frag can't be packed")
            c7 = self.om
        elif not self.frag:
            c7 = '0'
        elif len(self.frag) == 1:
            c7 = self.frag.lower()
        else:
            raise DesigError(self.unpacked +
                " can't pack 2 character fragment with provisional designation")
        return '{}{:02d}{}{}{}{}'.format(
            chr(self.cen - 10 + ord('A')),
            self.yr,
            self.hm,
            tens,
            self.num % 10,
            c7)

    @property
    def packed12(self):
        return '    ' + self.orbit + self.packed7


class ProvS:
    """Provisional natural satellite designations"""
    def __init__(self, cen, yr, planet, num):
        self.cen = int(cen)
        self.yr = int(yr)
        self.planet = planet
        self.num = int(num)

    @property
    def unpacked(self):
        return 'S/{:02d}{:02d} {} {}'.format(
            self.cen, self.yr, self.planet, self.num)

    @property
    def packed7(self):
        try:
            return '{}{:02d}{}{}{}0'.format(
                chr(self.cen - 10 + ord('A')),
                self.yr,
                self.planet,
                n62[self.num // 10],
                self.num % 10)
        except IndexError:
            raise DesigError(self.unpacked + ' too large for 7 character packed format')


def parseProvU(u):
    """parse an unpacked provisional designation

    Parameters
    ----------
    u : string
        minor panet, comet, or satellite designation in unpacked form

    Returns
    -------
    MProv, PProv, TProv, CProv, or SProv
    """
    m = rxProvM.fullmatch(u)  # typical minor planet
    if m:
        return ProvM(*m.groups())

    m = rxProvC.fullmatch(u)  # comet
    if m:
        return ProvC(*m.group(1, 2, 3, 4, 5, 6, 8))

    m = rxProvP.fullmatch(u)  # P-L survey
    if m:
        return ProvP(m.group(1))

    m = rxProvT.fullmatch(u)  # T survey
    if m:
        return ProvT(*m.group(1, 2))

    m = rxProvS.fullmatch(u)  # satellite
    if m:
        return ProvS(*m.groups())

    raise DesigError('invalid provID ' + u)


hm = string.ascii_uppercase[:25].replace('I', '')  # half month
om = string.ascii_uppercase.replace('I', '')  # order in month
rxProvM = re.compile(r'(\d\d)(\d\d) (['+hm+'])(['+om+r'])(\d*)')
rxProvP = re.compile(r'(\d+) P-L')
rxProvT = re.compile(r'(\d+) T-([123])')
rxProvC = re.compile(r'([PCDX])/(\d\d)(\d\d) (['+hm+'])(['+om+'])?(\d*)?'
    r'(-([A-Z]+))?')
rxProvS = re.compile(r'S/(\d\d)(\d\d) ([JSUN]) (\d+)')
