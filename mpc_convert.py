#!/usr/bin/env python3
"""Stuff useful to multiple pipeline python modules that deal with conversions
   from old to new formats."""

import os
import re
import string
import mpc_lib as ml
import mpc_astro as ma
import mpc_constants as mc
from datetime import datetime
from datetime import timedelta


# regex checkers for designations and observatory codes

rx_packeddes = re.compile(r'\A(([IJKL][0-9]{2}[A-HJ-Y][0-9A-Za-z][0-9][A-Z])|((PLS|T1S|T2S|T3S)[0-9]{4}))\Z')
"""Match the form of a packed minor planet designation.
"""

rx_packedcmtdes = re.compile(r'\A(A|C|P|D|X)(([0-9A-KLPT./\-][0-9]{2}[A-HJ-Y][0-9A-Za-z][0-9][A-Z0])|((PLS|T1S|T2S|T3S)[0-9]{4}))\Z')
"""Match the form of a packed comet designation.
"""

rx_packedsatdes = re.compile(r'\AS(([JKL][0-9]{2}[J|S|U|N][0-9A-Za-z][0-9][0])|((PLS|T1S|T2S|T3S)[0-9]{2}))\Z')
"""Match the form of packed nat sat designation.
"""

rx_packedpermdes = re.compile(r'\A([JSUN][0-9]{3}S)|([0-9]{4}[PD])|([0-9A-Za-z][0-9]{4})\Z')
### rx_packedpermdes = re.compile(r'\AS([JSUN][0-9]{3}S)|([0-9]{4}[PD])|([0-9A-Za-z][0-9]{4})\Z')
"""Match the form of a packed mp/cmt/natsat permanent designation.
"""

rx_newstylepackedprovdes = re.compile(r'\A([ACDPX]*?)(([IJK][0-9]{2}[A-HJ-Y][0-9]{5}[A-HJ-Z0])|((PLS|T1S|T2S|T3S)[0-9]{7}))\Z')
"""Match the form of a new-style packed prov desig.
"""

rx_obscode = re.compile(r'\A([0-9A-Z][0-9]{2}|XXX)\Z')


OBS_MODES = [('A', 'PHO'), ('C', 'CCD'), ('D', ''),
             ('E', ''), ('M', 'MIC'), ('N', 'MIC'),
             ('O', ''), ('P', 'PHO'), ('T', 'MER'), ('c', 'CCD'),
             ('d', ''), ('e', 'ENC'), ('n', 'VID'),
             ('X', ''), ('x', ''), ('V', 'CCD'), ('S', 'CCD'),
             ('H', 'PMT') ]


AST_CATS = [('a', 'USNOA1'), ('b', 'USNOSA1'), ('c', 'USNOA2'),
            ('d', 'USNOSA2'), ('e', 'UCAC1'), ('f', 'Tyc1'),
            ('g', 'Tyc2'), ('h', 'GSC1.0'), ('i', 'GSC1.1'),
            ('j', 'GSC1.2'), ('k', 'GSC2.2'), ('l', 'ACT'),
            ('m', 'GSCACT'), ('n', 'SDSS8'), ('o', 'USNOB1'),
            ('p', 'PPM'), ('q', 'UCAC4'), ('r', 'UCAC2'),
            ('s', 'USNOB2'), ('t', 'PPMXL'), ('u', 'UCAC3'),
            ('v', 'NOMAD'), ('w', 'CMC14'), ('x', 'Hip2'),
            ('y', 'Hip1'), ('z', 'GSC'), ('A', 'AC'),
            ('B', 'SAO1984'), ('C', 'SAO'), ('D', 'AGK3'),
            ('E', 'FK4'), ('F', 'ACRS'), ('G', 'LickGas'),
            ('H', 'Ida93'), ('I', 'Perth70'), ('J', 'COSMOS'),
            ('K', 'Yale'), ('L', '2MASS'), ('M', 'GSC2.3'),
            ('N', 'SDSS7'), ('O', 'SSTRC1'), ('P', 'MPOSC3'),
            ('Q', 'CMC15'), ('R', 'SSTRC4'), ('S', 'URAT1'),
            ('T', 'URAT2'), ('U', 'Gaia1'), ('V', 'Gaia2'),
            ('W', 'UCAC5') ]


ODD_REFS = [('HTCDR', 'HipTycCDRom'), ('JPLRS', 'JPLRadarSite'),  ('LowOC', 'LowellObsCir')]

EXPAND_REFS = [('AA', 'A\&A'),
                   ('AAS', 'A\&AS'),
                   ('AB', 'AbaOBS'),
                   ('Ab', 'AnOB'),
                   ('AC', 'AZWis'),
                   ('AE', 'aena.rpt'),
                   ('aJ', 'ApJ'),
                   ('an', 'AsNot'),
                   ('AP', 'ApJS'),
                   ('APO', 'AnPOb'),
                   ('AS',  'AcASn'),
                   ('AZ', 'AZh'),
                   ('As', 'A\&AS'),
                   ('BA', 'BuAst'),
                   ('BB', 'BAORB'),
                   ('BC', 'BAICz'),
                   ('BG', 'BOBeo'),
                   ('BN', 'BAN'),
                   ('BP', 'BSASP'),
                   ('BZ', 'ANZi'),
                   ('CB', 'CBOAA'),
                   ('CC', 'OACSC'),
                   ('CD', 'TsSta'),
                   ('CK', 'IzKry'),
                   ('CM', 'COMar'),
                   ('CO', 'IzOde'),
                   ('CPO', 'PerOC'),
                   ('CR', 'CRAS'),
                   ('CS', 'SoSht'),
                   ('HA', 'AnHar'),
                   ('HD', 'VeHei'),
                   ('IAP', 'IAPPP'),
                   ('Ic', 'Icar'),
                   ('JB', 'JBAA'),
                   ('JC', 'JASAC'),
                   ('KB', 'BuKwa'),
                   ('KF', 'KFNT'),
                   ('KK', 'CCCUK'),
                   ('LB', 'LicOB'),
                   ('LO', 'LowOB'),
                   ('LP', 'PLPla'),
                   ('MN', 'MNRAS'),
                   ('MP', 'M\&P'),
                   ('MPB', 'MPBu'),
                   ('NA',  'AnNic'),
                   ('NC', 'NihOC'),
                   ('NO', 'PUSNO'),
                   ('NZ', 'NAZ'),
                   ('OB', 'Obs'),
                   ('PA', 'PASP'),
                   ('PC', 'TsPul'),
                   ('PD', 'PTarO'),
                   ('PK', 'PKAO'),
                   ('PO', 'PerOC'),
                   ('PP', 'IzPul'),
                   ('PSS', 'P\&SS'),
                   ('PT', 'POTor'),
                   ('PZ', 'TsPul'),
                   ('RJ', 'RoAJ'),
                   ('RM', 'MmRAS'),
                   ('SA', 'MNSAA'),
                   ('SAL', 'SvAL'),
                   ('SRM', 'SRMO'),
                   ('SS', 'SoSyR'),
                   ('TB', 'TokAB'),
                   ('TC', 'TvOC'),
                   ('TI', 'IAOIT'),
                   ('UC',  'CiUO'),
                   ('WO', 'USNOA'),
                   ('WiA', 'AnWie'),
                   ('pM', 'MiPul'),
                   ('pA', 'CoSka') ]


def decode_MPC1992_orbit(els):
    """Decode a MPC1992-format orbit and return the basic orbit elements

    Parameters:
    -----------
    els : str
    A 220-col or 255-col element line in the MPC1992 format

    Returns:
    --------
    List containing (in order): T; Peri., Node, Incl., q, e

    """

    T = convert_packed_date_to_jd(els[12:24])

    value = []
    value.append(T)
    for i in range(24, 65, 10):
        if (i < 45):
            temp = els[i:i+3] + "." +els[i+3:i+10]
        else:
            if (i == 54):
                if "." in els[i:i+10]:
                    temp = els[i:i+10]
                else:
                    temp = els[i] + "." + els[i+1:i+10]
            else:
                temp = els[i] + "." + els[i+1:i+10]

        value.append(float(temp))

    return value


def convert_packed_date_to_jd(pdate):
  """Convert a 5- or 12-character packed date into a JD

  Parameters
  ----------
  pdate : str
    A 5- or 12-character packed date

  Returns
  -------
  jd : float
    The JD corresponding to the packed date.

  Note
  ----
  Works with all packed dates after the year -1500.
  If the length of the passed string is not 5 or 12, JD = 0 is returned.
  The old packed_date had two representations for dates in the year 0: "/00CV" and "000CV", both represent Dec. 31 in the year 0.
  For dates before 0, the characters "-", "." and "/" represent the years -200, -100 and 0, respectively.  And (year MOD 100) has
  be subtracted from those values.

  Examples
  --------
  >>> convert_packed_date_to_jd("K156R")
  2457200.5
  >>> convert_packed_date_to_jd("-395P1179600")
  1633907.61796

  """

  jd = 0.0
  l = len(pdate)
  d_frac = 0.0

  if l in [ 5, 12 ]:
    if pdate[0] >= "0":
      y = ml.base62_to_int(pdate[0]) * 100 + int(pdate[1:3])

    else:
      y = (ord(pdate[0]) - 47) * 100 - int(pdate[1:3])

    m = ml.base62_to_int(pdate[3])
    d = ml.base62_to_int(pdate[4])

    if l == 12:
      d_frac = int(pdate[5:]) / 10000000.0

    d += d_frac
    jd = ma.to_julian_date(y,m,d)

  return jd


def convert_jd_to_packed_date5(jd):

  yr, mo, day = ma.from_julian_date(jd)

  t8 = f'{yr:4d}{mo:02d}{int(day):02d}'

  return pack_char(t8[0:2]) + t8[2:4] + pack_char(t8[4:6]) + pack_char(t8[6:])


def pack_char(c):

  if type(c) is int:
    i = c
  else:
    i = int(c)

  if i < 10:
    return str(i)
  elif i < 36:
    return chr(i + 55)
  else:
    return chr(i + 61)


def convert_packed_perm_desig(desig):
  """"Convert a packed mp/cmt/sat permanent designation from old style to new style.

  Parameters
  ----------
  desig : str
    The old format packed designation string.  All old-style permanent designations
    are 5 chars.
 
  Returns
  -------
  new_desig : str
    The new format packed designation string.

  Examples
  --------

  """

  new_desig = ""
  if (len(desig) == 5):

    if (desig[0] in [ 'J', 'S', 'U', 'N' ]) and (desig[4] == "S"):
      new_desig = desig[0] + "0000" + desig[1:]

    else:
      if desig[0] in mc.VALID_CHARS[10:]:    # Is mp designation > 99999
        i = mc.VALID_CHARS.find(desig[0])
        new_desig = "000" + str(i) + desig[1:]

      else:
        new_desig = "0000" + desig

  return new_desig


def deconvert_packed_perm_desig(desig):
  """Convert a new-style packed mp/cmt/sat permanent deisgnation to old style.

  Parameters
  ----------
  desig : str
    The new-style packed mp/cmt/sat permanent designation.

  Returns
  -------
  old_desig : str
    The old format packed designation string.
  """

  if desig[-1] in ['P', 'D', 'C', 'A', 'I']:
    old_desig = desig[-5:]
  elif desig[-1] == "S":
    old_desig = desig[0] + desig[-4:]
  else:
    i = int(desig)
    if i > 99999:
      old_desig = ml.int_to_base62(int(desig[-6:-4]), 1) +  desig[-4:]
    else:
      old_desig = desig[-5:]

  return old_desig


def deconvert_packed_prov_desig(desig):
  """Convert a packed mp/cmt/sat provisional designation from new style to old style.

  Parameters
  ----------
  desig : str
    The new format packed designation string

  Returns
  -------
  old_desig : str
    The old format packed designation string.

  Examples
  --------
  >>> deconvert_packed_prov_des('K00A00000A')
  'K00A00A'
  >>> convert_packed_prov_desig('K00A00001A')
  'K00A01A'
  >>> convert_packed_prov_desig('K00A00010A')
  'K00A10A'
  >>> convert_packed_prov_desig('K00A00100A')
  'K00AA0A'
  >>> convert_packed_prov_desig('K00A00365A')
  'K00Aa5A'
  >>> convert_packed_prov_desig('K00A000010')
  'K00A010'
  >>> convert_packed_prov_desig('PK00A000010')
  'PK00A010'
  """

  old_desig = ""
  len_desig = len(desig)

  if re.match(rx_newstylepackedprovdes, desig):
    ptr = 4                         
    if (len(desig) == 11):
      ptr = 5

    if desig[ptr - 4] not in ['I', 'J', 'K']:
      if desig[ptr - 4:ptr - 1] in ["PLS", "T1S", "T2S", "T3S"]:              # First catch mp survey designations
        old_desig = desig[0:ptr - 1] + desig[ptr + 2:]

    else:

      if (desig[ptr:ptr + 3] == "000"):
        old_desig = desig[0:ptr] + desig[ptr + 3:]

      else:
        i = int(desig[ptr:ptr + 4])
        t3 = ml.int_to_base62(i, 3)
        if (t3[0:2] == "00"): 
          old_desig = desig[0:ptr] + t3[2] + desig[ptr + 4:]

  return old_desig


def convert_packed_prov_desig(desig):
  """Convert a packed mp/cmt/sat provisional designation from old style to new style.

  Parameters
  ----------
  desig : str
    The old format packed designation string.  Mp designations should be the full 7 chars.
    Cmt and sat designations can either be 7 or the full 8 chars.
 
  Returns
  -------
  new_desig : str
    The new format packed designation string.

  Examples
  --------
  >>> convert_packed_prov_desig('K00A00A')
  'K00A00000A'
  >>> convert_packed_prov_desig('K00A01A')
  'K00A00001A'
  >>> convert_packed_prov_desig('K00A10A')
  'K00A00010A'
  >>> convert_packed_prov_desig('K00AA0A')
  'K00A00100A'
  >>> convert_packed_prov_desig('K00Aa5A')
  'K00A00365A'
  >>> convert_packed_prov_desig('K00A010')
  'K00A000010'
  >>> convert_packed_prov_desig('PK00A010')
  'PK00A000010'
  """

  new_desig = ""
  len_desig = len(desig)

  if ((desig[0:3] in ["PLS", "T1S", "T2S", "T3S"]) and (len_desig == 7)):              # First catch mp survey designations
    new_desig = desig[0:3] + "000" + desig[3:] 


  else:                                                     # Deal with mp non-survey and non-mp designations
    if (desig[-1] >= "a"):           # Replace fragment ID with "0", there are no two-char fragment IDs among the provisional designations
      desig = desig[:-1] + "0"
 
    ptr = 4                         
    if (len(desig) == 8):
      ptr = 5

    new_desig = desig[0:ptr] + "000" + desig[ptr:]

    if desig[ptr] > "9":
      sequence_num = 10 * ml.decode_base_62(desig[ptr]) + int(desig[ptr+1])
      t5 = str(sequence_num).zfill(5)
      new_desig = new_desig[0:ptr] + t5 + new_desig[ptr+5:]

  return new_desig


def decode_reference(ref):
    """Decode an old style reference."""

    new_ref = "Unknown"
    if ref[0] == 'E':
        new_ref = 'MPEC ' + ref[1:7]
        t3 = f'{ml.base62_to_int(ref[7]):2d}' + ref[8]
        new_ref += t3.replace(" 0","").strip()

    elif ref[0] in string.ascii_lowercase and (ref[1] == '0' or ref[1:].isdigit()):
        new_ref = 'MPS ' + str(unpack_mps_number(ref)).rjust(8)

    elif ref[0] == '~':
        new_ref = 'MPS ' + str(260000 + ml.decode_base_62(ref[1:])).rjust(8)

    elif ref[0] == "@":
        new_ref = 'MPC   10' + ref[1:]

    elif ref[0] == "#":
        mps_num = ml.decode_base_62(ref[1:]) + 110000
        new_ref = 'MPC ' + str(mps_num).rjust(8)
    
    elif ref[0] == 'H' and (ref[1:].isdigit() or ref[1] == '0'):
        new_ref = 'HarAC ' + ref[1:].rjust(4)

    elif ref[0] == 'I' and (ref[1:].isdigit() or ref[1] == '0'):
        new_ref = 'IAUC ' + ref[1:].rjust(5)

    elif ref[0] == 'M' and (ref[1:].isdigit() or ref[1] == '0'):
        new_ref = 'MPC ' + ref[1:].rjust(8)

    elif ref[0] == 'R' and (ref[1:].isdigit() or ref[1] == '0'):
        new_ref = 'RI ' + ref[1:].rjust(4)

    elif ref.isdigit():
        new_ref = 'MPC ' + ref.rjust(8)

    elif ref[0:2] == 'GO':
        new_ref = 'GOAMM 1' + ref[2:]

    elif ref[0:2] == 'BJ':
        new_ref = 'BerAJ 1' + ref[2:]

    elif ref[0:3] in ['APO', 'AcA', 'AAS', 'CMC', 'CPO', 'IAP', 'IHW', 'LOB', 'MPB', 'PSS', 'RMA', 'SAL',  'SFO', 'SOB', 'SRM', 'WiA']:
        num = int(ref[3:])
        new_ref = ref[0:3] + ' ' + str(num).rjust(4)

    elif ref[0:2].isalpha():
        if ref[2].isdigit():
            num = int(ref[2:])
            new_ref = ref[0:2] + ' ' + str(num).rjust(4)

    if new_ref == "Unknown":
        for odd_ref in ODD_REFS:
            if ref in odd_ref[0]:
                new_ref = odd_ref[1]

    # Expand uncommon journal names

    if new_ref != "Unknown":
        new_ref_bits = new_ref.split()
        for expand_ref in EXPAND_REFS:
            if new_ref_bits[0] in expand_ref[0]:
                new_ref = expand_ref[1] + " " + new_ref_bits[1]

    return new_ref.replace(' 0', ' ')


def pack_reference(new_ref):
  """Attempt to convert a new-style reference to the obs80-style reference"""

  ref = ""

  bits = new_ref.split()
  journal = bits[0]

  for odd_ref in ODD_REFS:
    if journal in odd_ref[1]:
      ref = odd_ref[0]

  if ref == "":
    for expand_ref in EXPAND_REFS:
      if journal == expand_ref[1]:
        ref = expand_ref[0]
        lref = len(ref)
        ref += str(bits[1]).zfill(5-lref)
        break

  if ref == "":
    if journal == "GOAMM":
      ref = "GO" + bits[1][1:].zfill(3)
    if journal == "BerAJ":
      ref = "BJ" + bits[1][1:].zfill(3)
    if journal == "RI":
      ref = "R" + str(bits[1]).zfill(4)
    if journal == "IAUC":
      ref = "I" + str(bits[1]).zfill(4)
    if journal == "HarAC":
      ref = "H" + str(bits[1]).zfill(4)
    if journal in [ "AcA", "CMC", "IHW", "LOB", "RMA", "SFO", "SOB" ]:
      ref = journal + str(bits[1]).zfill(2)

  if ref == "":
    if len(journal) == 2:
      ref = journal + str(bits[1]).zfill(3)

  if journal == "MPC":
    mpc_num = int(bits[1])
    if mpc_num < 10000:
      ref = "M" + str(mpc_num).zfill(4)
    elif mpc_num < 100000:
      ref = str(mpc_num)
    elif mpc_num < 110000:
      ref = "@" + str(mpc_num - 100000).zfill(4)
    else:
      ref = "#" + ml.int_to_base62(mpc_num - 110000, 4)

  if journal == "MPS":
    mps_num = int(bits[1])
    if mps_num < 260000:
      ref = chr(97 + int(mps_num / 10000)) + str(mps_num % 10000).zfill(4)
    else:
      ref = "~" + ml.int_to_base62(mps_num - 260000, 4)        
      
  return ref


def is_mpc_old_packed_desig(desig):
  """Check if a designation is in MPC packed provisional designation format.

  Examples
  --------
  >>> is_mpc_old_packed_desig('K14A00A')
  True
  >>> is_mpc_old_packed_desig('K14Ma1N')
  True
  >>> is_mpc_old_packed_desig('J99T010')
  True
  >>> is_mpc_old_packed_desig('K14A00')     # too short
  False
  >>> is_mpc_old_packed_desig('K14A00A1')   # too long
  False
  >>> is_mpc_old_packed_desig('z14A00A')    # can't start with lowercase char
  False
  >>> is_mpc_old_packed_desig('KA4A00A')    # year invalid
  False
  """
  # Look for something like: XnnY, where X and Y are chars and nn is a num:

  year_code = "./0123456789ABCDEFGHIJKL"

  if (len(desig) == 7 and
      desig[0] in year_code and
      desig[1:3].isdigit() and
      desig[3] in string.ascii_uppercase and
      desig[6] in string.ascii_uppercase):
    return True

  else:
    return False


def unpack_mps_number(packed_num):
  """Unpack an MPS number.

  Examples
  --------
  >>> unpack_mps_number("j8391")
  98391
  >>> unpack_mps_number("a0320")
  320
  >>> unpack_mps_number("k0001")
  100001
  """

  encoded_number = {}

  for i in range(0, 26):
    char = string.ascii_lowercase[i]
    encoded_number[char] = i * 10000

  return encoded_number[packed_num[0]] + int(packed_num[1:])


def decode_ra_decl(str_in):
  """Decode MPC1992 R.A. and Decl. values

  Return the decimal value corresponding to the given R.A./Decl. string,
  the number of valid dps in the returned value and the precision in either
  seconds of arc or seconds of time. Declination values must start with
  either "+" or "-".
  """

  is_decl = False
  sign = 1.0                 # Store sign
  if str_in[0] in ['-', '+']:
    if str_in[0] == "-":
      sign = -1.0
    str = str_in[1:].strip()
    is_decl = True
  else:
    str = str_in.strip()

  if str[2] != " ":
    str = (str[0:2] + " " + str[3:]).strip()    # Remove fragment number (if present)
  
  ndp = 5                    # Determine number of valid decimal places
  pos_prec = 0.01
  if len(str) == 12:
    ndp = 6
    pos_prec = 0.001
  elif len(str) == 10:
    ndp = 4
    pos_prec = 0.1
  elif len(str) == 8:
    if str[5] == ".":
      ndp = 3
      pos_prec = 0.6
    else:
      ndp = 3
      pos_prec = 1.0
  elif len(str) == 7:
    ndp = 2
    pos_prec = 6.0
  elif len(str) == 5:
    ndp = 1
    pos_prec = 60.0

  if is_decl:
      ndp += 1
      
  str_elems = str.split(" ")  # Split string on spaces
  value = 0
  for i in range(0,len(str_elems)):
    value += float(str_elems[i])/(60.0**i)  # Determine value
  
  return value * sign, ndp, pos_prec


def decode_time_of_obs(str):
  """Decode MPC1992 date of observation

  Return the corresponding JD and the number of valid dps in the returned value
  """ 

  ndp = len(str.strip())-11        # Determine number of decimal places
  
  str_elems = str.split(" ")  # Split string on spaces
  jd = ma.to_julian_date(int(str_elems[0]), int(str_elems[1]), float(str_elems[2]))

  return jd, ndp


def decode_ast_cat(str):
    """Convert astrometric catalogue code to a readable string

    Return a string describing a specific astrometric catalogue based on the offered code.

    Returned string is limited to 8 characters by ADES specification document.

    """

    astcat = "Unknown"
    mpcobs_astcat = ""

    for cat in AST_CATS:
        if str in cat[0]:
            astcat = cat[1]

    return astcat


def pack_ast_cat(str):
    """Convert astrometric catalogue code string to the obs80 single char representation

    Return a character describing a specific astrometric catalogue based on the offered code.

    """

    astcat = " "
    mpcobs_astcat = ""

    for cat in AST_CATS:
        if str in cat[1]:
            astcat = cat[0]

    return astcat


def decode_obs_mode(str):
    """Convert obs mode to a readable string

    Return a string representation of the MPC1992 obs mode character.

    String is limited to 3 characters by ADES specification document.

    Some MPC1992 obs mode characters map to empty strings (see ADES).
    In conversion: "A" -> "PHO", any errors (very small in number) can
    be fixed manually; "O" -> "", as there are no examples currently
    in the files.

    """

    obsmode = "UNK"

    for mode in OBS_MODES:
      if str in mode[0]:
        obsmode = mode[1]

    return obsmode


def pack_obs_mode(str):
    """Convert readable-string obs mode to the MPC1992 obs mode character.

    Since some MPC1992 obs mode characters map to empty strings and others map to the
    same readable string (see ADES), it is not possible to convert back with 100% accuracy.
    But such cases will be infrequent and can be recovered from other ADES fields.

    """

    obsmode = " "

    if str == "CCD":
      obsmode = "C"
    else:
      for mode in OBS_MODES:
        if str in mode[1]:
          obsmode = mode[0]

    return obsmode


def unpacked_to_packed_desig(desig_in):
  """Convert unpacked designation to old-style packed designation

  Convert unpacked provisional or permanent designation to old-style unpacked designation

  Parameters
  ----------
  desig : str
    A provisional or permanent IAU-style designation for a minor planet or comet.
    No support at present time natural satellites.

  Returns
  -------
  A string containing the old-style packed provisional designation
  corresponding to desig
  """

  new_desig = ""

  desig = desig_in.strip()
  if (desig[0] == "(") and (desig[-1] == ")"):
    try:
      mp_num = int(desig[1:-1])
    except:
      mp_num = 0
    if mp_num > 0:
      if mp_num < 100000:
        new_desig = f'{mp_num:05d}'
      else:
        new_desig = ml.int_to_base62(int(mp_num / 10000), 1) + f'{mp_num % 10000:04d}'
        
  elif desig[0:2] in [ "C/", "D/", "P/", "X/", "A/" ]:
    try:
      year = int(desig[2:6])
      ptr = 6
      if desig[ptr] == " ": ptr = 7
      if desig[ptr] in [ 'A', 'B' , 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y' ]:
        try:
          monnum = int(desig[ptr+1:])
          if monnum < 100:
            t2 = f'{monnum:02d}'
          else:
            t2 = ml.int_to_base62(int(monnum / 10), 1) + f'{monnum % 10:1d}'
          new_desig = desig[0] + ml.int_to_base62(int(year / 100), 1) + f'{year % 100:02d}' + desig[ptr] + t2 + "0" 
        except:
          if desig[ptr+1] in [ 'A', 'B' , 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' ]:
            subscript = -1
            if len(desig) > (ptr + 2):
              try:
                subscript = int(desig[ptr+2:])
              except:
                subscript = -1
            else:
              subscript = 0
            if subscript > -1:
              if subscript < 100:
                t2 = f'{subscript:02d}'
              else:
                t2 = ml.int_to_base62(int(subscript / 10), 1) + f'{subscript % 10:1d}'
              new_desig = desig[0] + ml.int_to_base62(int(year / 100), 1) + f'{year % 100:02d}' + desig[ptr] + t2 + desig[ptr+1]
    except:
      year = 0

  elif desig[-1] in [ "P", "D"]:
    try:
      cmt_num = int(desig[0:-1])
    except:
      cmt_num = 0
    if cmt_num > 0:
      new_desig = f'{cmt_num:04d}{desig[-1]}'
      
  if new_desig == "":
    if desig[-3:] == "P-L":
      try:
        mp_num = int(desig[0:-3])
      except:
        mp_num = 0
      if mp_num > 0:
        new_desig = "PLS" + f'{mp_num:04d}'
    elif desig[-3:] == "T-1":
      try:
        mp_num = int(desig[0:-3])
      except:
        mp_num = 0
      if mp_num > 0:
        new_desig = "T1S" + f'{mp_num:04d}'
    elif desig[-3:] == "T-2":
      try:
        mp_num = int(desig[0:-3])
      except:
        mp_num = 0
      if mp_num > 0:
        new_desig = "T2S" + f'{mp_num:04d}'
    elif desig[-3:] == "T-3":
      try:
        mp_num = int(desig[0:-3])
      except:
        mp_num = 0
      if mp_num > 0:
        new_desig = "T3S" + f'{mp_num:04d}'
    else:
      desig = desig.replace(" ","")
      if desig[0] == "A":
        desig = "1" + desig[1:]
      try:
        mp_num = int(desig[0:4])
      except:
        mp_num = 0

      if (mp_num > 1800) and (mp_num < 2100):
        desig2 = ml.int_to_base62(int(mp_num / 100), 1) + f'{mp_num % 100:02d}'
        if desig[4] in "ABCDEFGHJKLMNOPQRSTUVWXY":
          if desig[5] in "ABCDEFGHJKLMNOPQRSTUVWXYZ":
            t2 = "00"
            if len(desig) > 6:
              try:
                subscript = int(desig[6:])
                if subscript < 100:
                  t2 = f'{subscript:02d}'
                else:
                  t2 = ml.int_to_base62(int(subscript / 10), 1) + f'{subscript % 10:1d}'
              except:
                t2 = ""

            if t2 != "":
              new_desig = desig2 + desig[4] + t2 + desig[5]

  return new_desig


def packed_to_unpacked_desig(desig):
  """Convert old-style packed designation to unpacked designation

  Convert old-style packed provisional or permanent designation to unpacked designation

  Parameters
  ----------
  desig : str
    A packed provisional or permanent designation for a minor planet,
    comet or nat sat

  Returns
  -------
  A string containing the unpacked designation corresponding to the
  given packed provdes/permdes.
  If the offered designation is not valid, an empty string is returned.
    
  """

  if type(desig) is int:
    desig = str(desig)

  new_desig = ""
  t10 = ""
  t10a = ""
  is_sat = False

  l = len(desig)

  if l == 5:
    tags = [ ('J', 'Jupiter'), ('S', 'Saturn'), ('U', 'Uranus'), ('N', 'Neptune') ]
    for tag in tags:
      if tag[0] in desig[0] and desig[4] == "S":
        new_desig = tag[1] + " " + ml.to_roman(int(desig[1:4]))

    if new_desig == "":
      if desig[4] in [ 'P', 'D', 'X', 'I']:
        new_desig = str(int(desig[0:4])) + desig[4]

      else:
        if desig[0] >= "A":
          new_desig = "(" + str(ml.base62_to_int(desig[0])) + desig[1:5] + ")"
        else:
          new_desig = "(" + str(int(desig)) + ")"

  else:
    if l == 7:
      t10 = desig

    elif l == 8:
      if desig[0] in ['P', 'C', 'D', 'A', 'X']:
        if desig[7].islower():
          desig = desig[0:7] + "0"
        new_desig = desig[0] + "/"
        t10 = desig[1:]

      elif desig[0] == "S":
        new_desig = desig[0] + "/"
        t10 = desig[1:]
        is_sat = True

    if t10 != "":
      tags = [ ("PLS", "P-L"), ("T1S", "T-1"), ("T2S", "T-2"), ("T3S", "T-3") ]

      for tag in tags:            # Deal with survey designations
        if tag[0] in t10[:3]:
          t10a = str(int(t10[3:])) + " " + tag[1]

      if t10a == "":              # Deal with non-survey designations
        if t10[0] >= "0":
          t1 = str(ml.base62_to_int(t10[0]))
          if t1 == "0":
            t1 = ""
          t10a = t1 + t10[1:3]+ " " + t10[3]
          if is_sat:
            t10a += " "
        else:
          t10a = "-"
          if t10[0] == ".":
            t10a += "1"
          elif t10[0] == "-":
            t10a += "2"
          t10a += str(99 - int(t10[1:3])).zfill(2) + " " + t10[3]

        if new_desig == "" and t10[0:3] < "J25":
          t10a = "A" + t10a[1:]

        if t10[-1] != "0": t10a += t10[-1]

        if t10[4:6] != "00":
          if t10[4:6] <= "99":
            t10a += str(int(t10[4:-1]))
          else:
            t10a += str(int(ml.base62_to_int(t10[4]))) + t10[5]
            
      new_desig += t10a

  return new_desig


def convert_program_code(old_code):

  new_code = ""
  if old_code != "":
    offset = mc.OLD_PROGRAM_CODES.find(old_code)
    if offset > -1:
      new_code = ml.int_to_base62(offset, 2)

  return new_code


def pack_program_code(new_code):

  T62 = "0123456789!\"#$%&'()*+,-./[\]^_`{|}~:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

  old_code = ""
  if new_code != "":
    offset = ml.base62_to_int(new_code)
    old_code = T62[offset]

  return old_code



def convert_to_obstime_new(obsdate):

  obsdate = obsdate.rstrip()
  yy = obsdate[0:4]
  mm = obsdate[5:7]
  dd = obsdate[8:10]
  pd = obsdate[10:]
  dt = datetime(int(yy),int(mm),int(dd)) + timedelta(days=float(pd))
#  obstime = dt.strftime('%Y-%m-%dT%H:%M:%S') 
#  dc = "%.2f" % float(dt.strftime('.%f')) + "Z"
#  obstime += dc.lstrip("0")
  obstime=dt.strftime("%Y-%m-%dT%H:%M:%S") + "." + str(round(dt.microsecond, -3))[:3] + "Z"
  return obstime
  

def convert_to_obstime(obsdate):

  obs = obsdate.rstrip()
  l = len(obs)

  obstime = ""
  if (l > 11) and (l < 18):
    obstime = obs[0:4] + "-" + obs[5:7] + "-" + obs[8:10] + "T"
    n_csecs = 86400.0 * float(obs[10:]) + 0.001
    hrs = n_csecs / 3600.0
    n_csecs -= int(hrs) * 3600.0
    mins = n_csecs / 60.0
    secs = n_csecs - int(mins) * 60.0
#    print(n_csecs, l)
#    print(hrs, mins, secs)
#    if l == 12:
#      obstime += str(int(hrs)).zfill(2)
#    elif l == 13:
#      obstime += "{:04.1f}".format(hrs)
#    elif l == 14:
#      obstime += str(int(hrs)).zfill(2) + ":" + str(int(mins)).zfill(2)
#    elif l == 15:
#      t4 = "{:04.1f}".format(mins)
#      if t4 == "60.0":
#        t4 = "00.0"
#        hrs += 1
#        if hrs >= 24.0:
#          print(obsdate, hrs, mins, secs, 1)
#      obstime += str(int(hrs)).zfill(2) + ":" + t4
#    elif l == 16:
    if l < 17:
      secs += 0.5
      if secs >= 60.0:
        secs -= 60.0
        mins += 1.0
        if mins >= 60.0:
          mins -= 60.0
          hrs += 1
          if hrs >= 24.0:
            print(obsdate, hrs, mins, secs, 1) 
#      print(hrs, mins, secs)
      obstime += str(int(hrs)).zfill(2) + ":" + str(int(mins)).zfill(2) + ":" + str(int(secs)).zfill(2)
    else:
      t4 = "{:04.1f}".format(secs)
      if t4 == "60.0":
        t4 = "00.0"
        mins += 1.0
        if mins > 60.0:
          mins -= 60.0
          hrs += 1.0
          if hrs > 24.0:
            print(obsdate, hrs, mins, secs, 2)
#      print(hrs, mins, secs)
      obstime += str(int(hrs)).zfill(2) + ":" + str(int(mins)).zfill(2) + ":" + t4
    obstime += "Z"

  return obstime  


if __name__ == '__main__':
  import doctest
  doctest.testmod()
#  doctest.testfile('mpclib_test.rst')
